package services;

import exceptions.FamilyOverflowException;
import people.Family;
import people.Human;
import pets.Pet;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FamilyService {
    private final int FAMILY_MEMBERS_LIMIT = 5;

    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void displayAllFamilies() {
        printFamilies(getAllFamilies());
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > numberOfMembers)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < numberOfMembers)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int numberOfMembers) {
        return (int) familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == numberOfMembers)
                .count();
    }

    public Family createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
        return newFamily;
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        Random random = new Random();
        String name = random.nextBoolean() ? boyName : girlName;
        family.addChild(new Human(name, family.getFather().getSurname(), getCurrentDate()));
        if (family.countFamily() > FAMILY_MEMBERS_LIMIT) {
            throw new FamilyOverflowException("Too many members!");
        }
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        if (family.countFamily() > FAMILY_MEMBERS_LIMIT) {
            throw new FamilyOverflowException("Too many members!");
        }
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        getAllFamilies().forEach(family -> family.setChildren(family.getChildren().stream()
                .filter(child -> child.getAge() <= age)
                .collect(Collectors.toList())));
    }

    public int count() {
        return getAllFamilies().size();
    }

    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public List<Pet> getPets(int index) {
        return index >= 0 && index < getAllFamilies().size() ?
                new ArrayList<>(getAllFamilies().get(index).getPets()) :
                new ArrayList<>();
    }

    public void addPet(int index, Pet pet) {
        if (index >= 0 && index < getAllFamilies().size()) {
            getAllFamilies().get(index).getPets().add(pet);
        }
    }

    public boolean loadData(List<Family> families) {
        return familyDao.loadData(families);
    }

    public boolean readData() {
        return familyDao.readData();
    }

    private String getCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return currentDate.format(formatter);
    }

    public void printFamilies(List<Family> families) {
        families.forEach(family -> System.out.println(family.prettyFormat()));
    }
}
