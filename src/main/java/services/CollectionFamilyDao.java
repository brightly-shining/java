package services;

import people.Family;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private final String DATABASE_FILE_PATH = "database.ser";
    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return index >= 0 && index < families.size() ? families.get(index) : null;
    }

    @Override
    public boolean deleteFamily(int index) {
        return index >= 0 && index < getAllFamilies().size() && families.remove(index) != null;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        int index = families.indexOf(family);
        if (index == -1) {
            families.add(family);
        } else {
            families.set(index, family);
        }
    }

    @Override
    public boolean loadData(List<Family> families) {
        try {
            FileOutputStream fileStream = new FileOutputStream(DATABASE_FILE_PATH);
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(families);
            objectStream.close();
            fileStream.close();
            return true;
        } catch (IOException ignore) {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public boolean readData() {
        try {
            FileInputStream fileStream = new FileInputStream(DATABASE_FILE_PATH);
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            families = (List<Family>) objectStream.readObject();
            objectStream.close();
            fileStream.close();
            return true;
        } catch (IOException | ClassNotFoundException ignore) {
            return false;
        }
    }
}
