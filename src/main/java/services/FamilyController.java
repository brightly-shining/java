package services;

import exceptions.FamilyOverflowException;
import people.Family;
import people.Human;
import pets.Pet;

import java.util.List;

public class FamilyController {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers) {
        return familyService.getFamiliesBiggerThan(numberOfMembers);
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers) {
        return familyService.getFamiliesLessThan(numberOfMembers);
    }

    public int countFamiliesWithMemberNumber(int numberOfMembers) {
        return familyService.countFamiliesWithMemberNumber(numberOfMembers);
    }

    public Family createNewFamily(Human mother, Human father) {
        return familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        try {
            return familyService.bornChild(family, boyName, girlName);
        } catch (FamilyOverflowException ignore) {
            return null;
        }
    }

    public Family adoptChild(Family family, Human child) {
        try {
            return familyService.adoptChild(family, child);
        } catch (FamilyOverflowException ignore) {
            return null;
        }
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public List<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public boolean loadData(List<Family> families) {
        return familyService.loadData(families);
    }

    public boolean readData() {
        return familyService.readData();
    }

    public void printFamilies(List<Family> families) {
        families.forEach(family -> System.out.println(family.prettyFormat()));
    }
}
