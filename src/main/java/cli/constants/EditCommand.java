package cli.constants;

public final class EditCommand {
    public static final String BORN_CHILD = "1";
    public static final String ADOPT_CHILD = "2";
    public static final String RETURN = "3";
}
