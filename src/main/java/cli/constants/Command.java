package cli.constants;

final public class Command {
    public static final String EXIT = "exit";
    public static final String LOAD_FAMILIES = "1";
    public static final String DISPLAY_ALL_FAMILIES = "2";
    public static final String DISPLAY_FAMILIES_BIGGER_THAN = "3";
    public static final String DISPLAY_FAMILIES_LESS_THAN = "4";
    public static final String COUNT_FAMILIES_WITH_MEMBER_NUMBER = "5";
    public static final String CREATE_NEW_FAMILY = "6";
    public static final String DELETE_FAMILY_BY_INDEX = "7";
    public static final String EDIT_FAMILY = "8";
    public static final String DELETE_CHILDREN_OLDER_THAN = "9";
    public static final String SAVE_FAMILIES = "10";
}
