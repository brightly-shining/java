package cli.constants;

public final class Strings {
    public static final String DESCRIPTION = (
            """
                    - 1. Завантажити збережений список сімей
                    - 2. Відобразити весь список сімей
                    - 3. Відобразити список сімей, де кількість людей більша за задану
                    - 4. Відобразити список сімей, де кількість людей менша за задану
                    - 5. Підрахувати кількість сімей, де кількість членів дорівнює
                    - 6. Створити нову родину
                    - 7. Видалити сім'ю за індексом сім'ї у загальному списку
                    - 8. Редагувати сім'ю за індексом сім'ї у загальному списку
                       - 1. Народити дитину
                       - 2. Усиновити дитину
                       - 3. Повернутися до головного меню
                    - 9. Видалити всіх дітей старше віку
                    - 10. Зберегти список сімей
                    Для виходу введіть "Exit"
                    """
    );

    public static final String SUBMENU_DESCRIPTION = (
            """
                    - 1. Народити дитину
                    - 2. Усиновити дитину
                    - 3. Повернутися до головного меню
                    """
    );

    public static final String UNKNOWN_COMMAND = "Команду не знайдено. Будь ласка, спробуйте ще раз";
    public static final String BAD_INTEGER = "Будь ласка, введіть ціле число";
    public static final String EMPTY_INPUT = "Будь ласка, спробуйте ще раз";
    public static final String RETURNED_TO_MENU = "Повернення до головного меню";
    public static final String FAMILIES_NOT_FOUND = "Не знайдено ні однієї сім'ї";
    public static final String FAMILIES_SAVED = "Родини успішно збережені";
    public static final String FAMILIES_LOADED = "Родини успішно завантажені";
    public static final String IO_ERROR = "Файл бази даних не знайдено";
    public static final String FAMILY_OVERFLOW = "В поточну родину неможливо додати нову дитину";
    public static final String FAMILY_NOT_FOUND = "Родину з поточним порядковим номером не знайдено";
    public static final String FAMILY_CREATED = "Родину створено і додано до списку";
    public static final String CHILDREN_DELETED = "Дітей, старших за вказаний вік, видалено";
    public static final String CHILD_ADDED = "Дитину успішно додано до поточної родини";
    public static final String FAMILY_DELETED = "Родину успішно видалено";
    public static final String PROVIDE_AGE = "Введіть вік";
    public static final String PROVIDE_COMMAND = "Введіть команду";
    public static final String PROVIDE_FAMILY_INDEX = "Введіть порядковий номер сім'ї";
    public static final String PROVIDE_MEMBERS_NUMBER = "Введіть кількість членів сім'ї";
    public static final String PROVIDE_BOY_NAME = "Введіть ім'я хлопця";
    public static final String PROVIDE_GIRL_NAME = "Введіть ім'я дівчини";
    public static final String PROVIDE_NAME = "Введіть ім'я";
    public static final String PROVIDE_SURNAME = "Введіть прізвище";
    public static final String PROVIDE_DATE = "Введіть дату народження у форматі 25/10/1995";
    public static final String PROVIDE_INTELLIGENCE_QUOTIENT = "Введіть коефіцієнт інтелекту";
    public static final String FATHER = "Батько";
    public static final String MOTHER = "Мати";
    public static final String CHILD = "Дитина";
}
