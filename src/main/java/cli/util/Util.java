package cli.util;

import cli.constants.Strings;

import java.util.Scanner;

public final class Util {
    public static void printNewLine() {
        print("\n", false);
    }

    public static void print(String message) {
        print(message, true);
    }

    public static void print(String message, boolean insertNewLine) {
        if (insertNewLine) message += "\n";
        System.out.print(message);
    }

    public static int getUserIntegerInput(String message) {
        while (true) {
            try {
                return Integer.parseInt(getUserInput(message));
            } catch (NumberFormatException ignore) {
                print(Strings.BAD_INTEGER);
            }
        }
    }

    public static String getUserInput(String message) {
        print(appendExpectedInputIndicator(message), false);
        return getUserInput();
    }

    public static String getUserInput() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String userInput = scanner.nextLine().trim();
            if (!userInput.isEmpty()) return userInput;
            print(appendExpectedInputIndicator(Strings.EMPTY_INPUT), false);
        }
    }

    public static String appendExpectedInputIndicator(String message) {
        return message + ": ";
    }

    public static String prependNewLine(String message) {
        return "\n" + message;
    }
}
