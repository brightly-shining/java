package cli;

import cli.constants.Command;
import cli.constants.Criteria;
import cli.constants.EditCommand;
import cli.constants.Strings;
import cli.util.Util;
import people.Family;
import people.Human;
import services.CollectionFamilyDao;
import services.FamilyController;
import services.FamilyDao;
import services.FamilyService;

import java.util.List;
import java.util.Map;

public class App {
    private final FamilyController controller;
    private final Map<String, Runnable> commands = Map.of(
            Command.LOAD_FAMILIES, this::handleLoadFamilies,
            Command.DISPLAY_ALL_FAMILIES, this::handleDisplayAllFamilies,
            Command.DISPLAY_FAMILIES_BIGGER_THAN, () -> handleDisplayFilteredFamilies(Criteria.BIGGER),
            Command.DISPLAY_FAMILIES_LESS_THAN, () -> handleDisplayFilteredFamilies(Criteria.LESS),
            Command.COUNT_FAMILIES_WITH_MEMBER_NUMBER, this::handleCountFamiliesWithMemberNumber,
            Command.CREATE_NEW_FAMILY, this::handleCreateNewFamily,
            Command.DELETE_FAMILY_BY_INDEX, this::handleDeleteFamilyByIndex,
            Command.EDIT_FAMILY, this::handleEditFamily,
            Command.DELETE_CHILDREN_OLDER_THAN, this::handleDeleteChildrenOlderThan,
            Command.SAVE_FAMILIES, this::handleSaveFamilies
    );

    public App() {
        FamilyDao dao = new CollectionFamilyDao();
        FamilyService service = new FamilyService(dao);
        this.controller = new FamilyController(service);
    }

    public void run() {
        while (true) {
            Util.print(Strings.DESCRIPTION);
            String userInput = Util.getUserInput(Strings.PROVIDE_COMMAND).toLowerCase();
            if (userInput.equals(Command.EXIT)) break;

            if (commands.containsKey(userInput)) {
                commands.get(userInput).run();
            } else {
                Util.print(Strings.UNKNOWN_COMMAND);
            }
            Util.printNewLine();
        }
    }

    private void handleSaveFamilies() {
        boolean succeeded = controller.loadData(controller.getAllFamilies());
        if (succeeded) {
            Util.print(Strings.FAMILIES_SAVED);
        } else {
            Util.print(Strings.IO_ERROR);
        }
    }

    private void handleLoadFamilies() {
        Util.print(controller.readData() ? Strings.FAMILIES_LOADED : Strings.IO_ERROR);
    }

    private void handleDisplayAllFamilies() {
        printFamilies(controller.getAllFamilies());
    }

    private void handleDeleteChildrenOlderThan() {
        int childAge = Util.getUserIntegerInput(Strings.PROVIDE_AGE);
        controller.deleteAllChildrenOlderThan(childAge);
        Util.print(Strings.CHILDREN_DELETED);
    }

    private void handleEditFamily() {
        outer:
        while (true) {
            Util.print(Strings.SUBMENU_DESCRIPTION);
            String userInput = Util.getUserInput(Strings.PROVIDE_COMMAND);

            switch (userInput) {
                case EditCommand.BORN_CHILD -> handleBornChild();
                case EditCommand.ADOPT_CHILD -> handleAdoptChild();
                case EditCommand.RETURN -> {
                    Util.print(Strings.RETURNED_TO_MENU);
                    break outer;
                }
                default -> Util.print(Strings.UNKNOWN_COMMAND);
            }
            Util.printNewLine();
        }
    }

    private void handleAdoptChild() {
        Family family = getUserFamilyInput();
        if (family == null) return;

        Family returnedFamily = controller.adoptChild(family, createHuman(Strings.CHILD));
        if (returnedFamily != null) {
            Util.print(Util.prependNewLine(Strings.CHILD_ADDED));
        } else {
            Util.print(Strings.FAMILY_OVERFLOW);
        }
    }

    private void handleBornChild() {
        Family family = getUserFamilyInput();
        if (family == null) return;

        String boyName = Util.getUserInput(Strings.PROVIDE_BOY_NAME);
        String girlName = Util.getUserInput(Strings.PROVIDE_GIRL_NAME);

        Family returnedFamily = controller.bornChild(family, boyName, girlName);
        if (returnedFamily != null) {
            Util.print(Util.prependNewLine(Strings.CHILD_ADDED));
        } else {
            Util.print(Strings.FAMILY_OVERFLOW);
        }
    }

    private void handleDeleteFamilyByIndex() {
        int index = Util.getUserIntegerInput(Strings.PROVIDE_FAMILY_INDEX) - 1;
        if (controller.deleteFamilyByIndex(index)) {
            Util.print(Strings.FAMILY_DELETED);
        } else {
            Util.print(Strings.FAMILIES_NOT_FOUND);
        }
    }

    private void handleCreateNewFamily() {
        controller.createNewFamily(createHuman(Strings.FATHER), createHuman(Strings.MOTHER));
        Util.print(Util.prependNewLine(Strings.FAMILY_CREATED));
    }

    private void handleCountFamiliesWithMemberNumber() {
        int numberOfMembers = Util.getUserIntegerInput(Strings.PROVIDE_MEMBERS_NUMBER);
        Util.print(String.valueOf(controller.countFamiliesWithMemberNumber(numberOfMembers)));
    }

    private void handleDisplayFilteredFamilies(String criteria) {
        int numberOfMembers = Util.getUserIntegerInput(Strings.PROVIDE_MEMBERS_NUMBER);
        switch (criteria) {
            case Criteria.LESS -> printFamilies(controller.getFamiliesLessThan(numberOfMembers));
            case Criteria.BIGGER -> printFamilies(controller.getFamiliesBiggerThan(numberOfMembers));
        }
    }

    private Human createHuman(String humanName) {
        Util.print(Util.prependNewLine(humanName));
        String name = Util.getUserInput(Strings.PROVIDE_NAME);
        String surname = Util.getUserInput(Strings.PROVIDE_SURNAME);
        String dateOfBirth = Util.getUserInput(Strings.PROVIDE_DATE);
        int intelligenceCoefficient = Util.getUserIntegerInput(Strings.PROVIDE_INTELLIGENCE_QUOTIENT);
        return new Human(name, surname, dateOfBirth, intelligenceCoefficient);
    }

    private Family getUserFamilyInput() {
        int index = Util.getUserIntegerInput(Strings.PROVIDE_FAMILY_INDEX) - 1;
        Family family = controller.getFamilyByIndex(index);

        if (family == null) Util.print(Strings.FAMILY_NOT_FOUND);
        return family;
    }

    private void printFamilies(List<Family> families) {
        if (families.isEmpty()) {
            Util.print(Strings.FAMILIES_NOT_FOUND);
        } else {
            controller.printFamilies(families);
        }
    }
}
