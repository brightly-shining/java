package pets;

import java.util.Set;

public class Fish extends Pet {
    {
        setSpecies(Species.FISH);
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив!%n", getNickname());
    }
}
