package pets;

public enum Species {
    DOMESTIC_CAT, ROBO_CAT, DOG, FISH, UNKNOWN
}
