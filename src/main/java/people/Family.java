package people;

import pets.Pet;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Family implements Serializable {
    private final Human mother;
    private final Human father;
    private List<Human> children = new LinkedList<>();
    private Set<Pet> pets = new HashSet<>();

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) return false;
        children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public int countFamily() {
        return children.size() + 2;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children.forEach(child -> child.setFamily(null));
        children.forEach(child -> child.setFamily(this));
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        return "family: {" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    public String prettyFormat() {
        String spacer = "    ";
        StringBuilder builder = new StringBuilder("family:\n");
        builder.append(spacer).append("mother: ").append(mother).append(",\n");
        builder.append(spacer).append("father: ").append(father).append(",\n");

        if (!children.isEmpty()) {
            builder.append(spacer).append("children:\n");
            children.forEach(child -> builder.append(spacer).append(spacer).append(child).append(",\n"));
        }

        if (!pets.isEmpty()) {
            builder.append(spacer).append("pets:\n");
            pets.forEach(pet -> builder.append(spacer).append(spacer).append(pet).append(",\n"));
        }

        String text = builder.toString();
        if (text.endsWith(",\n")) {
            text = text.substring(0, text.length() - 2);
        }
        return text;
    }
}
