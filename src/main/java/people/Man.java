package people;

import pets.Pet;

import java.io.Serializable;
import java.util.Map;

public final class Man extends Human implements Serializable {
    public Man(String name, String surname, String dateOfBirth) {
        super(name, surname, dateOfBirth);
    }

    public Man(String name, String surname, String dateOfBirth, int intelligenceCoefficient, Map<String, String> schedule) {
        super(name, surname, dateOfBirth, intelligenceCoefficient, schedule);
    }

    @Override
    public void greetPets() {
        for (Pet pet : getFamily().getPets()) {
            System.out.println("Привіт, мій дорогий " + pet.getNickname());
        }
    }

    public void repairCar() {
        System.out.println("Лагоджу автомобіль");
    }
}
