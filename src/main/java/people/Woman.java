package people;

import pets.Pet;

import java.io.Serializable;
import java.util.Map;

public final class Woman extends Human implements Serializable {
    public Woman(String name, String surname, String dateOfBirth) {
        super(name, surname, dateOfBirth);
    }

    public Woman(String name, String surname, String dateOfBirth, int intelligenceCoefficient, Map<String, String> schedule) {
        super(name, surname, dateOfBirth, intelligenceCoefficient, schedule);
    }

    @Override
    public void greetPets() {
        for (Pet pet : getFamily().getPets()) {
            System.out.println("Привіт, мій солоденький " + pet.getNickname());
        }
    }

    public void doMakeup() {
        System.out.println("Роблю макіяж");
    }
}
