package people;

import pets.Pet;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Random;

public class Human implements Serializable {
    private String name;
    private String surname;
    private String dateOfBirth;
    private long birthDate;
    private int intelligenceCoefficient;
    private Map<String, String> schedule;
    private Family family;

    public Human() {
    }

    public Human(String name, String surname, String dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.birthDate = convertToUnixTimestamp(dateOfBirth);
    }

    public Human(String name, String surname, String dateOfBirth, int intelligenceCoefficient) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.birthDate = convertToUnixTimestamp(dateOfBirth);
        this.intelligenceCoefficient = intelligenceCoefficient;
    }

    public Human(String name, String surname, String dateOfBirth,
                 int intelligenceCoefficient, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.birthDate = convertToUnixTimestamp(dateOfBirth);
        this.intelligenceCoefficient = intelligenceCoefficient;
        this.schedule = schedule;
    }

    public int getAge() {
        LocalDate currentDate = LocalDate.now();
        Instant instant = Instant.ofEpochSecond(birthDate);
        LocalDate localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        Period period = Period.between(localDate, currentDate);
        return period.getYears();
    }

    public void describeAge() {
        LocalDate currentDate = LocalDate.now();
        Instant instant = Instant.ofEpochSecond(birthDate);
        LocalDate localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        Period period = Period.between(localDate, currentDate);

        int years = period.getYears();
        int months = period.getMonths();
        int days = period.getDays();

        System.out.printf("There are %d %s, %d %s and %d %s since %s%n",
                years, pluralize(years, "year"), months, pluralize(months, "month"),
                days, pluralize(days, "day"), dateOfBirth);
    }

    private String pluralize(int count, String unit) {
        return count == 1 ? unit : unit + "s";
    }

    private long convertToUnixTimestamp(String dateString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = dateFormat.parse(dateString);

            return date.getTime() / 1000;
        } catch (ParseException ignore) {
        }
        return 0;
    }

    public void greetPets() {
        for (Pet pet : family.getPets()) {
            System.out.println("Привіт, " + pet.getNickname());
        }
    }

    public void describePets() {
        for (Pet pet : family.getPets()) {
            String trickLevelMessage = pet.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";
            String message = "У мене є " +
                    pet.getSpecies() + ", йому " +
                    pet.getAge() + " років, він " + trickLevelMessage;
            System.out.println(message);
        }
    }

    public boolean feedPets(boolean isTimeToFeed) {
        for (Pet pet : family.getPets()) {
            if (isTimeToFeed) {
                pet.eat();
                return true;
            }

            int randomNumber = new Random().nextInt(101);
            if (pet.getTrickLevel() > randomNumber) {
                System.out.println("Хм... погодую я " + pet.getNickname());
                pet.eat();
                return true;
            } else {
                String message = String.format("Думаю, %s не голодний", pet.getNickname());
                System.out.println(message);
                return false;
            }
        }
        return false;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getIntelligenceCoefficient() {
        return intelligenceCoefficient;
    }

    public void setIntelligenceCoefficient(int intelligenceCoefficient) {
        this.intelligenceCoefficient = intelligenceCoefficient;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "human: {" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + dateOfBirth +
                ", intelligenceCoefficient=" + intelligenceCoefficient +
                ", schedule=" + schedule +
                '}';
    }
}
