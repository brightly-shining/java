package services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import people.Family;
import people.Human;
import people.Man;
import people.Woman;
import pets.Dog;
import pets.Pet;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyServiceTest {
    private FamilyService familyService;
    private Family family;

    @BeforeEach
    public void setUp() {
        CollectionFamilyDao dao = new CollectionFamilyDao();
        this.familyService = new FamilyService(dao);

        Woman mother = new Woman("Alice", "Smith", "01/01/1980");
        Man father = new Man("Bob", "Smith", "02/02/1985");
        family = this.familyService.createNewFamily(mother, father);
    }

    private void createNewFamily() {
        Human mother = new Human("Emma", "Taylor", "03/03/1990");
        Human father = new Human("David", "Taylor", "04/04/1995");
        familyService.createNewFamily(mother, father);
    }

    @Test
    public void testCreateAndRetrieveFamily() {
        List<Family> families = familyService.getAllFamilies();
        assertEquals(1, families.size());

        Family retrievedFamily = familyService.getFamilyByIndex(0);
        assertEquals(family, retrievedFamily);
        assertEquals(retrievedFamily, familyService.getFamilyByIndex(0));
    }

    @Test
    public void testAddAndRetrievePet() {
        Pet pet = new Dog("Rex");
        familyService.addPet(0, pet);

        List<Pet> pets = familyService.getPets(0);
        assertEquals(1, pets.size());
        assertEquals(pet, pets.get(0));
    }

    @Test
    public void testCount() {
        assertEquals(1, familyService.count());
        createNewFamily();
        assertEquals(2, familyService.count());
    }

    @Test
    public void testDeleteFamilyByIndex() {
        assertTrue(familyService.deleteFamilyByIndex(0));
        assertEquals(0, familyService.count());
        assertFalse(familyService.deleteFamilyByIndex(0));
    }

    @Test
    public void testGetFamiliesBiggerThan() {
        createNewFamily();
        assertEquals(2, familyService.getFamiliesBiggerThan(1).size());
        assertEquals(2, familyService.getFamiliesBiggerThan(1).size());
    }

    @Test
    public void testGetFamiliesLessThan() {
        createNewFamily();
        assertEquals(2, familyService.getFamiliesLessThan(3).size());
        assertEquals(0, familyService.getFamiliesLessThan(2).size());
    }

    @Test
    public void testCountFamiliesWithMemberNumber() {
        createNewFamily();
        assertEquals(2, familyService.countFamiliesWithMemberNumber(2));
        assertEquals(0, familyService.countFamiliesWithMemberNumber(3));
    }

    @Test
    public void testBornChild() {
        familyService.bornChild(family, "Tom", "Lily");
        assertEquals(3, family.countFamily());
        String childName = family.getChildren().get(0).getName();
        assertTrue(childName.equals("Tom") || childName.equals("Lily"));
    }

    @Test
    public void testAdoptChild() {
        Human child = new Human("Lucas", "Smith", "05/05/2010");
        familyService.adoptChild(family, child);
        assertTrue(family.getChildren().contains(child));
    }

    @Test
    public void testDeleteAllChildrenOlderThan() {
        familyService.bornChild(family, "Tom", "Lily");

        familyService.deleteAllChildrenOlderThan(10);
        assertEquals(1, family.getChildren().size());

        familyService.deleteAllChildrenOlderThan(-1);
        assertEquals(0, family.getChildren().size());
    }
}
