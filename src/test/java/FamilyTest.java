import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import people.Family;
import people.Human;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private Family family;

    @Test
    void deleteChildByIndex() {
        Human child = new Human();
        family.addChild(child);

        assertFalse(family.deleteChild(2));
        assertEquals(1, family.getChildren().size());
        assertTrue(family.deleteChild(0));
        assertEquals(0, family.getChildren().size());
    }

    @Test
    void deleteChildByObject() {
        Human firstChild = new Human();
        Human secondChild = new Human();
        family.addChild(firstChild);

        assertFalse(family.deleteChild(secondChild));
        assertEquals(1, family.getChildren().size());
        assertTrue(family.deleteChild(firstChild));
        assertEquals(0, family.getChildren().size());
    }

    @Test
    void addChild() {
        Human child = new Human();
        family.addChild(child);
        assertTrue(family.getChildren().contains(child));
    }

    @Test
    void countFamily() {
        Human child = new Human();
        family.addChild(child);
        assertEquals(3, family.countFamily());
        family.deleteChild(child);
        assertEquals(2, family.countFamily());
    }

    @Test
    void testToString() {
        String expected = "family: {" +
                "mother=human: {" +
                "name='Jill', surname='Smith', birthDate=25/05/1990, intelligenceCoefficient=0, schedule=null" +
                "}, father=human: {" +
                "name='Jack', surname='Smith', birthDate=22/05/1990, intelligenceCoefficient=0, schedule=null" +
                "}, children=[], pets=[]}";
        assertEquals(expected, family.toString());
    }

    @BeforeEach
    void setUp() {
        Human mother = new Human("Jill", "Smith", "25/05/1990");
        Human father = new Human("Jack", "Smith", "22/05/1990");
        family = new Family(mother, father);
    }
}