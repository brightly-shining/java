import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import people.Human;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {
    Human human;

    @Test
    void testToString() {
        String expected = "human: {"
                + "name='Bob', surname='Harris', birthDate=26/10/2000, intelligenceCoefficient=0, schedule=null}";
        assertEquals(expected, human.toString());
    }

    @BeforeEach
    void setUp() {
        human = new Human("Bob", "Harris", "26/10/2000");
    }
}