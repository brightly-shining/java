import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pets.Pet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PetTest {
    Pet pet;

    @Test
    void testToString() {
        String expected = "unknown: {nickname='Shiny', age=0, trickLevel=0, habits=null}";
        assertEquals(expected, pet.toString());
    }

    @BeforeEach
    void setUp() {
        pet = new Pet("Shiny") {

            @Override
            public void respond() {
                System.out.printf("Привіт, хазяїн. Я - %s. Я скучив!%n", getNickname());
            }
        };
    }
}